from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .serializers import CarSerializer
from .models import Car

class CarView(APIView):
     def post(self, request):
          serializer = CarSerializer(data=request.data)
          if serializer.is_valid():
               serializer.save()
               return Response(
                    {
                         "data": serializer.data
                    },
                    status=status.HTTP_201_CREATED
               )
          else:
               return Response(
                    {
                         "error": serializer.errors
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
     
     def get(self, request, pk=None):
          if pk:
               car = get_object_or_404(Car, pk=pk)
               serializer = CarSerializer(car)
               return Response(
                    {
                         "data": serializer.data
                    },
                    status=status.HTTP_200_OK
               )
          cars = Car.objects.all()
          serializer = CarSerializer(cars, many=True)
          return Response(
               {
                    "data": serializer.data
               },
               status=status.HTTP_200_OK
          )

     def put(self, request, pk=None):
          car = get_object_or_404(Car, pk=pk)
          serializer = CarSerializer(car, data=request.data)
          if serializer.is_valid():
               serializer.save()
               return Response(
                    {
                         "data": serializer.data
                    },
                    status=status.HTTP_200_OK
               )
          else:
               return Response(
                    {
                         "error": serializer.errors
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
     def delete(self, request, pk=None):
          car = get_object_or_404(Car, pk=pk)
          car.delete()
          return Response(
               {
                    "message": "Item Deleted"
               },
               status=status.HTTP_200_OK
          )