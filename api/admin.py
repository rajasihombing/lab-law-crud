from django.contrib import admin
from api.models import Car

# Register your models here.
class HomeAdmin(admin.ModelAdmin):
    list_display = (
        'manufaktur', 
        'nama', 
        )

admin.site.register(Car, HomeAdmin)