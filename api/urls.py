from django.urls import path
from .views import CarView
app_name = 'api'
urlpatterns = [
     path('car', CarView.as_view(), name='carView'),
     path('car/<int:pk>', CarView.as_view(), name='carViewWithId'),
]
