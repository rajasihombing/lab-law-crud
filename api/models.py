from django.db import models
import uuid

class Car(models.Model):
    manufaktur = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)

    def __str__(self):
        return self.manufaktur