# from asyncio import tasks
from locust import HttpUser, between, task, constant_throughput
import json

class Load_Testing(HttpUser):
    wait_time = between(1,3)
    @task
    def load(self):
        data = {
            "manufaktur": "Toyota",
            "nama": "Fortuner"
        }
        self.client.put('/api/car/1', json=data)


class Stress_Testing(HttpUser):
    wait_time = between(1,3)
    @task
    def stress(self):
        self.client.get('/api/car')

class Endurance_Testing(HttpUser):
    wait_time = between(1,3)
    @task
    def stress(self):
        data = {
            "username" : "rajasihombing",
            "password" : "rajasihombing",
            "grant_type" : "password",
            "client_id" : "007",
            "client_secret" : "Saya adalah mahasiswa LAW"
        }
        self.client.post('/oauth/token/', json=data)


        